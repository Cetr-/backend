const USER_RESOURCE_MAP = {};
const USER_CITY_MAP = {};

export function addSeenResourceForUser(userId, resourceId) {
  const resourceIdList = USER_RESOURCE_MAP[userId] || [];
  resourceIdList.push(resourceId);
  USER_RESOURCE_MAP[userId] = resourceIdList;
}

export function getSeenResourcesForUser(userId) {
  return USER_RESOURCE_MAP[userId] || [];
}

export function refreshResourcesSeenByUser(userId) {
  USER_RESOURCE_MAP[userId] = [];
}

export function setUserCity(userId, city) {
  if (!USER_CITY_MAP[userId]) {
    USER_CITY_MAP[userId] = {};
  }
  USER_CITY_MAP[userId].city = city;
}

export function setUserArea(userId, area) {
  if (!USER_CITY_MAP[userId]) {
    USER_CITY_MAP[userId] = {};
  }
  USER_CITY_MAP[userId].area = area;
}

export function getUserCityAndArea(userId) {
  return USER_CITY_MAP[userId];
}

export function refreshUserCityAndArea(userId) {
  delete USER_CITY_MAP[userId];
}
