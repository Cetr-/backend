import dotenv from 'dotenv';

dotenv.config();
export const SPREAD_SHEET_ID = process.env.SPREADSHEETS_ID;

export const {
  GOOGLE_SHEETS_API_KEY,
  TELE_BOT_API_TOKEN_LUCKNOW,
  TELE_BOT_API_TOKEN_DELHI,
  SECRET,
  GOOGLE_OAUTH_CLIENT_ID,
  GOOGLE_OAUTH_CLIENT_SECRET,
  COOKIE_KEY
} = process.env;

export const BOT_TO_CITY = {
  [process.env.TELE_BOT_API_TOKEN_LUCKNOW]: 'Lucknow',
  [process.env.TELE_BOT_API_TOKEN_DELHI]: 'NCR - Delhi',
};
