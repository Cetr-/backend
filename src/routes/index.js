import express from 'express';
import '../services/passport';
import authRoutes from './authRoutes';
import { fetchResourcesAndInsert, fetchResourcesFromSheet } from './resources';
import { SECRET } from '../settings';
import { SHEET_IDS, SUPPORTED_SHEET_TABS } from './sheetsFormats';
import requireLogin from '../middleware/requireLogin';

const indexRouter = express.Router();

authRoutes(indexRouter);

indexRouter.get('/', requireLogin, async (req, res) => {
  return res.status(200).json({ message: 'OK' });
});

indexRouter.get('/resources', async (req, res) => {
  if (!req.query.resourceName) {
    return res.status(400).json({
      message:
        'BAD_REQUEST please add a resource name in query e.g. ..?resourceName=Oxygen',
    });
  }

  const result = await fetchResourcesFromSheet(req.query.resourceName);
  return res.status(200).json({ message: result });
});

// Sync from sheet into the database.
indexRouter.get('/sync/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }

  if (!req.query.resourceName) {
    return res.status(400).json({
      message:
        'BAD_REQUEST please add a resource name in query e.g. ..?resourceName=Oxygen',
    });
  }

  const result = await fetchResourcesAndInsert(req.query.resourceName);
  return res.status(200).json({ message: result });
});

// Sync from volunteers sheet into the database.
indexRouter.get('/syncSheet/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }

  if (!req.query.sheetId) {
    return res.status(400).json({
      message: 'BAD_REQUEST Specify sheet id to sync the db',
    });
  }

  if (!req.query.sheetName) {
    return res.status(400).json({
      message: 'BAD_REQUEST specify the sheet tab name',
    });
  }

  const result = await fetchResourcesAndInsert(
    req.query.sheetName,
    req.query.sheetId
  );
  return res.status(200).json({ message: result });
});

indexRouter.get('/syncAll/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const result_per_sheet = {
    total_processed: 0,
    total_inserted: 0,
    total_errors: 0,
    total_updated: 0,
    error_records: [],
    inserted_records: [],
  };

  for (const sheetIdx in SHEET_IDS) {
    for (const tabIdx in SUPPORTED_SHEET_TABS) {
      console.log('Doing ', SUPPORTED_SHEET_TABS[tabIdx], SHEET_IDS[sheetIdx]);
      const result = await fetchResourcesAndInsert(
        SUPPORTED_SHEET_TABS[tabIdx],
        SHEET_IDS[sheetIdx]
      );

      console.log('Done', SUPPORTED_SHEET_TABS[tabIdx], SHEET_IDS[sheetIdx]);
      result_per_sheet.total_processed += result.total_processed || 0;
      result_per_sheet.total_updated += result.total_updated || 0;
      result_per_sheet.total_inserted += result.total_inserted || 0;
      result_per_sheet.total_errors += result.total_errors || 0;
      result_per_sheet.error_records = [
        ...result_per_sheet.error_records,
        ...result.error_records,
      ];
      result_per_sheet.inserted_records = [
        ...result_per_sheet.inserted_records,
        ...result.inserted_records,
      ];
    }
  }

  res.status(200).json({ message: result_per_sheet });
});

export default indexRouter;
