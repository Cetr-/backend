import { Sequelize } from 'sequelize';
import { Resource } from '../../db/resource';

/** Find distinct cities in db. From user point of view "city" means "state". This is a misnomer in code*/
export async function getAllCities() {
  const results = await Resource.findAll({
    attributes: [[Sequelize.fn('DISTINCT', Sequelize.col('L_AREA')), 'City']],
    order: [['L_AREA', 'ASC']],
  });

  return {
    status: 200,
    // filter null and send only labels
    message: results
      .filter((res) => res.dataValues.City)
      .map((res) => res.dataValues.City),
  };
}

/** Find distinct areas per larger are in db */
export async function getSubCity({ city }) {
  const results = await Resource.findAll({
    where: { L_AREA: city },
    attributes: [
      [Sequelize.fn('DISTINCT', Sequelize.col('L_SUBAREA')), 'Area'],
    ],
    order: [['L_SUBAREA', 'ASC']],
  });

  return {
    status: 200,
    // filter null and send only labels
    message: [
      'all',
      ...results
        .filter((res) => res.dataValues.Area)
        .filter((res) => res.dataValues.Area !== 'all')
        .map((res) => res.dataValues.Area),
    ],
  };
}
