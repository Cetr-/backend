import { Op } from 'sequelize';
import { Resource } from '../../db/resource';

export function findResourcesByTypeAndLocation(request) {
  if (!request.type || !request.L_AREA) {
    return {
      status: 400,
      message: 'BAD_REQUEST please add a type and state.',
    };
  }
  const areaWise = {};
  if (request.L_AREA) {
    areaWise.L_AREA = request.L_AREA.trim().toLowerCase();
  }
  if (request.L_SUBAREA) {
    areaWise.L_SUBAREA = request.L_SUBAREA.trim().toLowerCase();
  }

  const resourceQuery = {
    where: {
      [Op.and]: [
        { NAME: request.type },
        {
          [Op.and]: [ areaWise ],
        },
      ],
    },
    order: [
      [ 'LAST_UPDATED', 'DESC' ],
      [ 'VERIFICATION_COUNT', 'DESC' ],
    ],
  };

  return Resource.findAll(resourceQuery).then((resources) => {
    if (resources.length === 0) {
      return {
        status: 404,
        resources: {
          values: [],
          message: `No resource of type ${request.type} found in ${
            request.L_AREA
          } ${request.L_SUBAREA ? `,${request.L_SUBAREA}` : '.'}`,
        },
      };
    }

    return {
      status: 200,
      resources: {
        values: resources,
      },
    };
  });
}
