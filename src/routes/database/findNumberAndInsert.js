import { FindRecordByNumber, Create, UpdateOldValues } from '../../db/resource';

function getUpdateFields(dbRec, newRec) {
  const updateRec = {
    id: dbRec.id,
    shouldUpdate: false,
  };
  const updateFields = {};
  // Update state
  if (dbRec.L_AREA !== newRec.L_AREA.getFormatted()) {
    updateFields.L_AREA = newRec.L_AREA.getFormatted();
    updateRec.shouldUpdate = true;
  }
  // Update city
  if (dbRec.L_SUBAREA !== newRec.L_SUBAREA.getFormatted()) {
    updateFields.L_SUBAREA = newRec.L_SUBAREA.getFormatted();
    updateRec.shouldUpdate = true;
  }
  // additional comments
  if (dbRec.ADDITIONAL_COMMENTS !== newRec.additionalComments) {
    updateFields.ADDITIONAL_COMMENTS = newRec.additionalComments;
    updateRec.shouldUpdate = true;
  }
  // subtype
  if (dbRec.SUBTYPE !== newRec.subType) {
    updateFields.SUBTYPE = newRec.subType;
    updateRec.shouldUpdate = true;
  }
  //subtype
  if (dbRec.STATUS !== newRec.status) {
    updateFields.STATUS = newRec.status;
    updateRec.shouldUpdate = true;
  }
  // last updated
  if (newRec.lastUpdated && dbRec.LAST_UPDATED !== newRec.lastUpdated) {
    updateFields.LAST_UPDATED = newRec.lastUpdated;
    updateRec.shouldUpdate = true;
  }
  updateRec.updateFields = updateFields;
  return updateRec;
}

/**
 * Checks for number in db and inserts them if absent.
 * @param {*} record
 * @returns
 */
export async function findAndInsertIfAbsent(record) {
  const formattedNumber = record.phoneNumbers.getFormatted();
  const res = await FindRecordByNumber(formattedNumber);
  if (res.length !== 0) {
    const updateRec = getUpdateFields(res[0].dataValues, record);
    if (!updateRec.shouldUpdate) {
      return {
        status: 'NOT_INSERTED',
        error: 'NUMBER_ALREADY_EXISTS',
        message: res.map((resource) => resource.dataValues),
      };
    }
    const updateMsg = await UpdateOldValues(
      updateRec.id,
      updateRec.updateFields
    );
    if (updateMsg.status === 201) {
      return {
        status: 'UPDATED',
        message: res.map((resource) => resource.dataValues),
      };
    }
    return {
      status: 'NOT_INSERTED',
      error: 'UNKNOWN_UPDATE_ERROR',
      message: 'Unknown reason',
    };
  }

  const createMsg = await Create(record);

  if (createMsg.status === 200) {
    return {
      status: 'INSERTED',
    };
  }
  return {
    status: 'NOT_INSERTED',
    error: 'UNKNOWN_ERROR',
    message: 'Unknown reason',
  };
}
