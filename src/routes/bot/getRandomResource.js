import { Op } from 'sequelize';
import { Resource } from '../../db/resource';
import { getWeightedRandom } from '../../utils/getWeightedRandom';
import {
  addSeenResourceForUser,
  getSeenResourcesForUser,
  getUserCityAndArea
} from '../../global/store';
import { CALLBACK_API, sendMessage } from './telegramBot';

function getRandomResourceOfType(request) {
  if (!request.type || !(request.city || request.L_AREA)) {
    return {
      status: 400,
      message:
        'BAD_REQUEST please add a resource name in query e.g. ..?type=Oxygen&city=lucknow',
    };
  }
  const areaWise = {};
  if (request.L_AREA) {
    areaWise.L_AREA = request.L_AREA;
  }
  if (request.L_SUBAREA) {
    areaWise.L_SUBAREA = request.L_SUBAREA;
  }

  const resourceQuery = {
    where: {
      [Op.and]: [
        { NAME: request.type },
        {
          [Op.or]: [
            { CITY: request.city },
            {
              [Op.and]: [ areaWise ],
            },
          ],
        },
      ],
    },
    order: [ [ 'VERIFICATION_COUNT', 'ASC' ] ],
  };

  if (request.userTelegramId) {
    const seenResources = getSeenResourcesForUser(request.userTelegramId);
    if (seenResources.length > 0) {
      resourceQuery.where.id = { [Op.notIn]: seenResources };
    }
  }
  return Resource.findAll(resourceQuery).then((resources) => {
    if (resources.length === 0) {
      return { status: 404, message: 'no resources returned' };
    }
    const randomResource = getWeightedRandom(resources);
    if (request.userTelegramId) {
      addSeenResourceForUser(request.userTelegramId, randomResource.id);
    }

    return {
      status: 200,
      message: randomResource,
    };
  });
}

/**
 * Get a random resource.
 *
 * @param {} msg
 * @param {*} name
 * @param {*} token
 * @returns
 */
export async function getRandomResource(msg, name, token) {
  const userTelegramId = msg.chat.id;
  const callback_json = {
    api: CALLBACK_API.UPDATE,
    query: {},
  };

  const CityAndArea = getUserCityAndArea(userTelegramId);

  const resource = await getRandomResourceOfType({
    type: name,
    city: CityAndArea.city,
    L_AREA: CityAndArea.city,
    L_SUBAREA: CityAndArea.area,
    userTelegramId,
  });

  if (resource.status === 404) {
    return sendMessage(
      token,
      msg.chat.id,
      `You have seen all resources for ${name}, please click on /list to see them all again!`
    );
  }

  const doneAction = JSON.stringify({
    ...callback_json,
    query: {
      id: resource.message.id,
      inc: 1,
      exit: true,
    },
  });
  const retryAction = JSON.stringify({
    ...callback_json,
    query: {
      id: resource.message.id,
      retry: name,
    },
  });

  const reportSpamAction = JSON.stringify({
    ...callback_json,
    query: {
      id: resource.message.id,
      dec: 1,
    },
  });

  const bookmarkAction = JSON.stringify({
    ...callback_json,
    query: {
      id: resource.message.id,
      userId: userTelegramId,
      bookmark: 1,
    }
  });

  const combined = resource.message.PHONE_NUMBER.split(',').map(
    (number) => `Click to call: ${number}\n`
  );

  return sendMessage(
    token,
    msg.chat.id,
    `<b>${resource.message.VENDOR_NAME}</b>\n<i>${resource.message.ADDITIONAL_COMMENTS}</i>\n${combined}`,
    {
      parse_mode: 'HTML',
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Retry another record?',
              callback_data: retryAction,
            },
          ],
          [
            {
              text: '✅ This worked',
              callback_data: doneAction,
            },
            {
              text: '❌ Report not working',
              callback_data: reportSpamAction,
            },
          ],
          [
            {
              text: '🔖 Bookmark this',
              callback_data: bookmarkAction,
            },
          ],
        ],
      },
    }
  );
}
