import express from 'express';
import { Resource } from '../../db/resource';
import { updateResources } from './updateResourceAction';
import { createResource } from './database/createResource';
import { getRandomResourceOfType } from './getRandomResource';
import { getResourceTypes } from './database/getResourceTypes';
import { telegramHandleCallBack, telegramHandleChat } from './telegramBot';
import { SECRET, BOT_TO_CITY } from '../../settings';

const FIRST_CITY = 'lucknow';

// BACKEND API ENPOINTS //
const botRouter = express.Router();

// Called from telegram to us
botRouter.post('/:authToken', async (req, res) => {
  if (!BOT_TO_CITY[req.params.authToken]) {
    res.send(401);
    return;
  }

  const token = req.params.authToken;
  if (req.body.callback_query) {
    console.log('callback query');
    const resp = await telegramHandleCallBack(req.body.callback_query, token);
    setResult(resp, res);
    return;
  }
  if (req.body.message) {
    console.log('req msg');
    const resp = await telegramHandleChat(req.body.message, token);
    setResult(resp, res);
    return;
  }

  setResult(
    {
      status: 200,
      message: '',
    },
    res
  );
});

// not related to telegram.
botRouter.get('/all-resources/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  Resource.findAll().then((resources) => res.json(resources));
});

botRouter.get('/types/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const response = await getResourceTypes({ city: FIRST_CITY });
  setResult(response, res);
});

botRouter.get('/randomResource/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const response = await getRandomResourceOfType({
    type: req.query.type,
    city: FIRST_CITY,
  });
  setResult(response, res);
});

botRouter.post('/resource/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const response = await createResource(req.body);
  setResult(response, res);
});

botRouter.post('/typeformSubmission/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const typeFormAnswers = req.body.form_response.answers;
  const record = {};
  typeFormAnswers.forEach((entry) => {
    switch (entry.field.id) {
      case 'uVnj62EEcUrV': // type of resource
        record.name = entry.choice.label;
        break;
      case 'Qnt0fc8OWg6B': // vendor name
        record.vendorName = entry.text;
        break;
      case 'VOz2U1IKQfCC': // phone number
        record.phoneNumbers = [entry.phone_number];
        break;
      case 'WqCNUs39We88': // additionalComments
        record.additionalComments = entry.text;
        break;
      case 'YSWW3TMsTFoo': // verified
        record.verified = entry.boolean;
        break;
      case 'BZssQuZNzq5q': //city
        record.city = entry.choice.label;
        break;
    }
  });

  const response = await createResource(record);
  setResult(response, res);
});

botRouter.put('/resource/:id/:authToken', async (req, res) => {
  if (req.params.authToken !== SECRET) {
    res.send(401);
    return;
  }
  const response = await updateResources({
    id: req.params.id,
    request: req.body,
  });
  setResult(response, res);
});

export function setResult(response, res) {
  res.status(response.status).json({
    message: response.message,
  });
}

export default botRouter;
