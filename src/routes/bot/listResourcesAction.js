import { CALLBACK_API, sendMessage } from './telegramBot';
import { getResourceTypes } from './database/getResourceTypes';
import {
  getUserCityAndArea,
  refreshResourcesSeenByUser,
} from '../../global/store';

/**
 * Calls database and send telegram a list of available resources.
 *
 * @param {*} chatId The chatId returned by telegram
 */
export async function listResources(msg, token) {
  refreshResourcesSeenByUser(msg.chat.id);
  const userTelegramId = msg.chat.id;
  const CityAndArea = getUserCityAndArea(userTelegramId);
  const typesOfResources = await getResourceTypes({
    city: CityAndArea.city,
    L_AREA: CityAndArea.city,
    L_SUBAREA: CityAndArea.area,
  });
  const callback_json = {
    api: CALLBACK_API.RESOURCE,
    query: {},
  };
  return sendMessage(
    token,
    msg.chat.id,
    'Select a resource, we will try to search for the most recent available record',
    {
      reply_markup: {
        inline_keyboard: typesOfResources.message.map((label) => [
          {
            text: label,
            callback_data: JSON.stringify(
              Object.assign(callback_json, { query: { name: label } })
            ),
          },
        ]),
      },
    }
  );
}
