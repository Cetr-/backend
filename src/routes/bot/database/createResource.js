import { Create } from '../../../db/resource';
import { convertRequestToResource } from '../../resources';

export function createResource(incomingRequest) {
  try {
    const resource = convertRequestToResource(incomingRequest);
    return Create(resource);
  } catch (e) {
    if (e.status) {
      return {
        status: e.status,
        message: e.message,
      };
    }

    return {
      status: 500,
      message: 'Internal error occured, contact developer',
      debugMessage: e,
    };
  }
}
