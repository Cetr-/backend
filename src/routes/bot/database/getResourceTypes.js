import { Sequelize, Op } from 'sequelize';
import { Resource } from '../../../db/resource';

export async function getResourceTypes(request) {
  const areaWise = {};
  if (request.L_AREA) {
    areaWise.L_AREA = request.L_AREA;
  }
  if (request.L_SUBAREA) {
    areaWise.L_SUBAREA = request.L_SUBAREA;
  }

  const results = await Resource.findAll({
    where: {
      [Op.or]: [
        { CITY: request.city },
        {
          [Op.and]: [ areaWise ],
        },
      ],
    },
    attributes: [ [ Sequelize.fn('DISTINCT', Sequelize.col('NAME')), 'Name' ] ],
  });

  return {
    status: 200,
    // filter null and send only labels
    message: results
      .filter((res) => res.dataValues.Name)
      .map((res) => res.dataValues.Name),
  };
}
