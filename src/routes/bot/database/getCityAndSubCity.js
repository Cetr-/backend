import { Sequelize } from 'sequelize';
import { Resource } from '../../../db/resource';

/** Find distinct cities in db */
export async function getAllCities(offset = 0) {
  const results = await Resource.findAll({
    attributes: [ [ Sequelize.fn('DISTINCT', Sequelize.col('L_AREA')), 'City' ] ],
    order: [ [ 'L_AREA', 'ASC' ] ],
    limit: 12,
    offset: 0 + offset,
  });

  return {
    status: 200,
    // filter null and send only labels
    message: results
      .filter((res) => res.dataValues.City)
      .map((res) => res.dataValues.City),
  };
}

/** Find distinct areas per city in db */
export async function getSubCity({ city, offset = 0 }) {
  const results = await Resource.findAll({
    where: { L_AREA: city },
    attributes: [
      [ Sequelize.fn('DISTINCT', Sequelize.col('L_SUBAREA')), 'Area' ],
    ],
    order: [ [ 'L_SUBAREA', 'ASC' ] ],
    offset: 0 + offset,
    limit: 12,
  });

  return {
    status: 200,
    // filter null and send only labels
    message: [
      'all',
      ...results
        .filter((res) => res.dataValues.Area)
        .filter((res) => res.dataValues.Area !== 'all')
        .map((res) => res.dataValues.Area),
    ],
  };
}
