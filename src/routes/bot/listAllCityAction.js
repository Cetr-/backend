import { CALLBACK_API } from './telegramBot';
import { getAllCities } from './database/getCityAndSubCity';
import { listPaginatedResource } from './sendPaginatedMessage';
import { refreshUserCityAndArea } from '../../global/store';

/**
 * Shows a list of available cities in the db in the paginated manner.
 *
 * @param {*} msg
 * @param {*} token
 * @param {*} callback_action If this is the result of clicking a button, some param/info will be passed.
 * @returns
 */
export function listAllCities(msg, token, callback_action = undefined) {
  const userTelegramId = msg.chat.id;
  refreshUserCityAndArea(userTelegramId);
  const newMsg = 'Before you begin, you must select a region you want to get a resource in';
  const action = callback_action || {
    api: CALLBACK_API.CITY,
    query: {
      os: 0,
    },
  };
  return listPaginatedResource(
    msg,
    token,
    (offset) => getAllCities(offset),
    action,
    CALLBACK_API.SUBCITY,
    newMsg
  );
}
