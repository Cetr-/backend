import { FindOrCreate } from '../../db/user';
import { CreateBookmark, FindBookmark } from '../../db/bookmark';

export async function addBookmark(userTelegramId, resourceId) {
  try {
    const user = await FindOrCreate(userTelegramId);
    const existingBookmark = await FindBookmark(user.id, resourceId);
    if (existingBookmark === null) {
      await CreateBookmark(user.id, resourceId);
    }
  } catch (error) {
    error.status = 500;
    throw error;
  }
}
