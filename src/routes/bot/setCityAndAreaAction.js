import { sendMessage } from './telegramBot';
import { helpMsg } from './helpAction';
import { getUserCityAndArea, setUserArea } from '../../global/store';

/**
 * Message banner to show the updated city and area.
 *
 * @param {*} msg
 * @param {*} token
 * @param {*} action  If this is the result of clicking a button, some param/info will be passed.
 * @returns
 */
export function setCityAndArea(msg, token, action = undefined) {
  const userTelegramId = msg.chat.id;
  if (action) {
    // If set to all, select all resources of the state.
    if (action.query.name !== 'all') {
      setUserArea(userTelegramId, action.query.name);
    }
  }
  return cityAndAreWelcomeMessage(msg, token);
}

/**
 * Welcome message to show the current city.
 *
 * @param {*} msg
 * @param {*} token
 * @returns
 */
export function cityAndAreWelcomeMessage(msg, token) {
  const userTelegramId = msg.chat.id;
  const currentCityArea = getUserCityAndArea(userTelegramId);
  if (!currentCityArea) {
    return sendMessage(
      token,
      msg.chat.id,
      'Oops your region got reset. Click /city to configure it again.'
    );
  }
  const cityAndAreaheaderMsg = `Your region has been set to ${
    currentCityArea.city
  }
      ${currentCityArea.area ? `and area to ${currentCityArea.area}` : ''}
      To change your location click /city
      `;

  return sendMessage(token, msg.chat.id, cityAndAreaheaderMsg).then((_) => helpMsg(msg, token));
}
