import { sendMessage } from './telegramBot';

/**
 * Display's feedback form for reporting bugs.
 *
 * @param {*} msg
 * @param {*} token
 * @returns
 */
export function feedback(msg, token) {
  return sendMessage(token, msg.chat.id, 'Report bugs or give feedback', {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: 'Report bugs and issues',
            url: 'https://forms.gle/WUzANNJBKfFVAMzd8',
          },
        ],
      ],
    },
  });
}
