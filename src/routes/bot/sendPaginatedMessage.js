import { sendMessage, editMessage } from './telegramBot';

const PAGE_LIMIT = 12;
const PAGINATION_STATE = {
  START: 'start',
  START_BACK: 'start_back',
  MID: 'mid',
  MID_END: 'mid_end',
  END: 'end',
};

/**
 * Convert a 1xN column list to 2 x N/2
 * This is useful for formatting the inline keyboard in telegram chat which is a NxN matrix of row x columns
 *
 * @returns
 */
function convertTo2xN(records, callback_json) {
  const columns = [];

  for (let i = 0; i < records.length; i += 2) {
    const row = [
      {
        text: records[i],
        callback_data: JSON.stringify(
          Object.assign(callback_json, { query: { name: records[i] } })
        ),
      },
    ];

    if (i + 1 < records.length) {
      row.push({
        text: records[i + 1],
        callback_data: JSON.stringify(
          Object.assign(callback_json, {
            query: { name: records[i + 1] },
          })
        ),
      });
    }
    columns.push(row);
  }

  return columns;
}

function getPaginationStage(action, offset, currentLen) {
  // It is a complete new message and users first attempt.
  if (!action.query.e) return PAGINATION_STATE.START;
  // No offset means starting state
  if (offset === 0) {
    return PAGINATION_STATE.START_BACK;
  }

  // If records returned are < pageSize, means we have ended
  if (currentLen > 0 && currentLen < PAGE_LIMIT) {
    return PAGINATION_STATE.MID_END;
  }

  // Tried a new page and no more records were fetched
  if (currentLen == 0) {
    return PAGINATION_STATE.END;
  }

  return PAGINATION_STATE.MID;
}

function getCallBackForPage(currentApi, counter) {
  const updatedQuery = Object.assign(currentApi.query, {
    os: counter,
    e: 1, // indicator to continue editing the message and not send a new one
  });

  return JSON.stringify({
    api: currentApi.api,
    query: updatedQuery,
  });
}

/**
 * Showing a list of any resource in a paginated manner.
 *
 * @param {*} msg
 * @param {*} token
 * @param {*} paginationApi
 * @param {*} action
 * @param {*} nextApi
 * @param {*} newMsg
 * @returns
 */
export async function listPaginatedResource(
  msg,
  token,
  paginationApi,
  action,
  nextApi,
  newMsg
) {
  const offset = action.query.os || 0;
  const results = await paginationApi(offset);
  const callback_json = {
    api: nextApi,
    query: {},
  };

  const availableOptions = convertTo2xN(results.message, callback_json);
  const paginationStage = getPaginationStage(
    action,
    offset,
    results.message.length
  );

  switch (paginationStage) {
  // Shows only NEXT button
  case PAGINATION_STATE.START:
    return sendMessage(token, msg.chat.id, newMsg, {
      reply_markup: {
        inline_keyboard: [
          ...availableOptions,
          [
            {
              text: 'Next>>',
              callback_data: getCallBackForPage(action, offset + PAGE_LIMIT),
            },
          ],
        ],
      },
    });
  case PAGINATION_STATE.START_BACK:
    return editMessage(token, msg.chat.id, msg.message_id, newMsg, {
      reply_markup: {
        inline_keyboard: [
          ...availableOptions,
          [
            {
              text: 'Next>>',
              callback_data: getCallBackForPage(action, offset + PAGE_LIMIT),
            },
          ],
        ],
      },
    });
    // Shows PREV and NEXT button
  case PAGINATION_STATE.MID:
    return editMessage(token, msg.chat.id, msg.message_id, newMsg, {
      reply_markup: {
        inline_keyboard: [
          ...availableOptions,
          [
            {
              text: '<<Prev',
              callback_data: getCallBackForPage(action, offset - PAGE_LIMIT),
            },
            {
              text: 'Next>>',
              callback_data: getCallBackForPage(action, offset + PAGE_LIMIT),
            },
          ],
        ],
      },
    });
    // Shows PREV  button only
  case PAGINATION_STATE.MID_END:
    return editMessage(token, msg.chat.id, msg.message_id, newMsg, {
      reply_markup: {
        inline_keyboard: [
          ...availableOptions,
          [
            {
              text: '<<Prev',
              callback_data: getCallBackForPage(action, offset - PAGE_LIMIT),
            },
          ],
        ],
      },
    });
    // Shows PREV button with an EOF text
  case PAGINATION_STATE.END:
    return editMessage(
      token,
      msg.chat.id,
      msg.message_id,
      'End of list reached! Please go back and select another city',
      {
        reply_markup: {
          inline_keyboard: [
            ...availableOptions,
            [
              {
                text: '<<Prev',
                callback_data: getCallBackForPage(
                  action,
                  offset - PAGE_LIMIT
                ),
              },
            ],
          ],
        },
      }
    );
  }
}
