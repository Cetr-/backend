import { sendMessage } from './telegramBot';

export function createMsg(msg, token) {
  return sendMessage(
    token,
    msg.chat.id,
    'Enter data in this form. Our volunteers will verify them before adding to our db',
    {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Add information',
              url: 'https://05ekv3ulj9q.typeform.com/to/odL1bq6b',
            },
          ],
        ],
      },
    }
  );
}
