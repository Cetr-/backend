import axios from 'axios';
import { createMsg } from './createAction';
import { update } from './updateResourceAction';
import { helpMsg } from './helpAction';
import { disclaimer } from './disclaimerAction';
import { feedback } from './feedbackAction';
import { getRandomResource } from './getRandomResource';
import { listResources } from './listResourcesAction';
import { listAllCities } from './listAllCityAction';
import { listAllSubCities } from './listAllSubCityAction';
import { setCityAndArea } from './setCityAndAreaAction';
import { getUserCityAndArea } from '../../global/store';
import { deleteBookmark, listBookmarks } from './bookmarksAction';

const TELEGRAM_BASE_URL = 'https://api.telegram.org/bot';
export const PAGE_LIMIT = 12;
// NEXT ID: 10
export const CALLBACK_API = {
  CITY: 3,
  SUBCITY: 4,
  LIST_RESOURCES: 5,
  RESOURCE: 6,
  UPDATE: 7,
  SET_CITY_AND_AREA: 8,
  DELETE_BOOKMARK: 9,
};

export function sendMessage(token, chatId, text, additional = {}) {
  return axios
    .post(`${TELEGRAM_BASE_URL}${token}/sendMessage`, {
      chat_id: chatId,
      text,
      ...additional,
    })
    .then((_) => ({
      status: 200,
      message: 'Done',
    }))
    .catch((error) => {
      console.error(error);
      return {
        status: 500,
        message: `An error occurred: ${error}`,
      };
    });
}

export function editMessage(token, chatId, messageId, text, additional = {}) {
  return axios
    .post(`${TELEGRAM_BASE_URL}${token}/editMessageText`, {
      chat_id: chatId,
      message_id: messageId,
      text,
      ...additional,
    })
    .then((_) => ({
      status: 200,
      message: 'Done',
    }))
    .catch((error) => ({
      status: 500,
      message: `An error occurred: ${error}`,
    }));
}

export function checkCity(msg, token) {
  const userTelegramId = msg.chat.id;
  const currentCityArea = getUserCityAndArea(userTelegramId);
  if (currentCityArea) {
    return true;
  }

  return false;
}

/**
 * Router logic for different call back/user action when a user
 * choses to click buttons in telegram chat. The callback data is generally set
 * on buttons set in response. Search for callback_data field.
 *
 * @param {*} telegram telegram bot instance
 * @param {*} callback_data
 */
async function callBackHandler(callback_data, token) {
  const action = JSON.parse(callback_data.data);
  const msg = callback_data.message;
  const isCitySet = checkCity(msg, token);
  switch (action.api) {
  case '/getRandomResource': // leave this in for backward compatibility
  case CALLBACK_API.RESOURCE:
    if (!isCitySet) {
      return listAllCities(msg, token);
    }
    return getRandomResource(msg, action.query.name, token);
  case '/update': // leave this in for backward compatibility
  case CALLBACK_API.UPDATE:
    if (!isCitySet) {
      return listAllCities(msg, token);
    }
    return update(msg, action.query, token);
  case CALLBACK_API.CITY:
    return listAllCities(msg, token, action);
  case CALLBACK_API.SUBCITY:
    return listAllSubCities(msg, token, action);
  case CALLBACK_API.SET_CITY_AND_AREA:
    return setCityAndArea(msg, token, action);
  case CALLBACK_API.DELETE_BOOKMARK:
    return deleteBookmark(msg, token, action);
  default:
    return sendMessage(
      token,
      msg.chat.id,
      'Thank you for letting us serve you today!'
    );
  }
}

/**
 * Handles telegram call back messages.
 * @param {*} callback_query  Telegram callback_query (unvalidated)
 * @returns
 */
export function telegramHandleCallBack(callback_query, token) {
  return callBackHandler(callback_query, token);
}

/**
 * Handles chat messages sent from user.
 * @param {*} msg Telegram msg json (unvalidated)
 * @returns
 */
export async function telegramHandleChat(msg, token) {
  const sentMessage = msg.text;
  // API's that do not need to check for city.
  if (sentMessage.match(/\/start/)) {
    return disclaimer(msg, token);
  }
  if (sentMessage.match(/create/gi)) {
    return createMsg(msg, token);
  }
  if (sentMessage.match(/\/feedback/)) {
    return feedback(msg, token);
  }
  if (sentMessage.match(/\/disclaimer/)) {
    return disclaimer(msg, token);
  }
  if (sentMessage.match(/\/city/)) {
    return listAllCities(msg, token);
  }
  if (sentMessage.match(/\/help/gi)) {
    return helpMsg(msg, token);
  }
  // APIs that need to check for city.
  const isCitySet = await checkCity(msg, token);
  if (!isCitySet) {
    return listAllCities(msg, token);
  }
  if (sentMessage.match(/\/list/)) {
    return listResources(msg, token);
  }
  if (sentMessage.match(/\/bookmarks/)) {
    return listBookmarks(msg, token);
  }

  return sendMessage(
    token,
    msg.chat.id,
    'Hello friend! I only respond to some commands. Click /help to understand how i can help you.'
  );
}
