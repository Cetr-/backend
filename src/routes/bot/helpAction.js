import { sendMessage } from './telegramBot';
import { getUserCityAndArea } from '../../global/store';

/**
 * Help message from bot.
 *
 * @param {*} msg Telegram msg
 * @param {*} token api token
 * @returns
 */
export function helpMsg(msg, token) {
  const userTelegramId = msg.chat.id;
  const city = getUserCityAndArea(userTelegramId)
    ? getUserCityAndArea(userTelegramId).city
    : 'your city';
  const helpMsg = `Welcome to the info bot for covid resources around ${city}!
              Click /list to show a list of available resources in and around ${city}!
              Click /create to add information about a valid resource that you know.
              Click /feedback to send bugs or errors for the bot.
              Click /disclaimer to read the disclaimer.
              Click /city to set your city
              Click /bookmarks to view all the resources you have bookmarked
  
              Please do give us correct and accurate information. 🙏 let's help each other in these hard times.`;
  return sendMessage(token, msg.chat.id, helpMsg);
}
