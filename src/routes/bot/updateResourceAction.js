import { FindOrCreate } from '../../db/user';
import { CreateVote, Find, UpdateVote } from '../../db/uservote';
import { Update } from '../../db/resource';
import { getRandomResource } from './getRandomResource';
import { sendMessage } from './telegramBot';
import { addBookmark } from './bookmarkAction';

function validateRequest(incomingRequest) {
  if (!incomingRequest.id) {
    throw { status: 400, message: 'No valid id set to update the record' };
  }

  if (!incomingRequest.request.verificationCount) {
    throw { status: 400, message: 'You can only update verificationCount' };
  }
}

function buildUpdateFields(request) {
  const updatedFields = {};
  if (request.verificationCount) {
    updatedFields.verificationCount = request.verificationCount;
  }

  return updatedFields;
}

/**
 * Update resources.
 * @param {*} incomingRequest
 * @returns
 */
export async function updateResources(incomingRequest) {
  try {
    validateRequest(incomingRequest);
    const { id: resourceId, request, userId: telegramId } = incomingRequest;
    const vote = request.verificationCount === 1;
    const user = await FindOrCreate(telegramId);
    const userVote = await Find(user.id, resourceId);
    const updateFields = buildUpdateFields(request);

    if (userVote === null) {
      await CreateVote(user.id, resourceId, vote);
      return Update(resourceId, parseInt(updateFields.verificationCount));
    }

    if (userVote.vote !== vote) {
      await UpdateVote(user.id, resourceId, vote);
      return Update(resourceId, parseInt(updateFields.verificationCount));
    }
    const message = `User has already ${
      vote ? 'up' : 'down'
    }voted this resource`;
    console.info(message);
    return { message, status: 200 };
  } catch (e) {
    if (e.status) {
      return e;
    }
    return {
      status: 500,
      message: 'Internal error occurred, contact developer',
    };
  }
}

/**
 * Update resource count
 * @param {*} msg
 * @param {*} query
 * @param {*} token
 * @returns
 */
export async function update(msg, query, token) {
  const userId = msg.chat.id;
  if (query.inc) {
    return updateResources({
      id: query.id,
      request: { verificationCount: 1 },
      userId,
    }).then((_) => sendMessage(
      token,
      msg.chat.id,
      'Thank you for reporting! This has been noted and we will mark this number as available! Type /help to see what else i can do to help.'
    ));
  }
  if (query.dec) {
    return updateResources({
      id: query.id,
      request: { verificationCount: -1 },
      userId,
    }).then((_) => sendMessage(
      token,
      msg.chat.id,
      'Thank you for reporting! This has been noted and we will decrease the rank of this number! Type /help to see what else i can do to help.'
    ));
  }

  if (query.retry) {
    return getRandomResource(msg, query.retry, token);
  }

  if (query.bookmark) {
    return addBookmark(query.userId, query.id)
      .then(() => sendMessage(
        token,
        msg.chat.id,
        'This resource has been added to your bookmarks! To view all click /bookmarks'
      ))
      .catch((e) => ({
        status: e.status,
        message: 'Something went wrong trying to create bookmark!',
      }));
  }
  if (query.exit) {
    return sendMessage(
      token,
      msg.chat.id,
      'Thank you for letting us serve you today! Type /help to see what else i can help.'
    );
  }
}
