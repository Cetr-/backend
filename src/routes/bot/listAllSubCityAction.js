import { setUserCity } from '../../global/store';
import { listPaginatedResource } from './sendPaginatedMessage';
import { getSubCity } from './database/getCityAndSubCity';
import { setCityAndArea } from './setCityAndAreaAction';
import { CALLBACK_API } from './telegramBot';

/**
 * If a city, like Delhi is divided into sub-regions - fetch and show them. Smaller cities like lucknow
 * that do not have sub-regions are not shown this message, and directed to welcome banner instead.
 *
 * @param {*} msg
 * @param {*} token
 * @param {*} callback_action  If this is the result of clicking a button, some param/info will be passed.
 * @returns
 */
export async function listAllSubCities(
  msg,
  token,
  callback_action = undefined
) {
  const userTelegramId = msg.chat.id;
  const newMsg = 'Select an area you want to get a resource in';
  const action = callback_action || {
    api: CALLBACK_API.SUBCITY,
    query: {
      name: action.query.name,
      os: 0,
    },
  };
  setUserCity(userTelegramId, action.query.name);
  if (!callback_action.query.e) {
    const area_results = await getSubCity({ city: action.query.name });
    // If this city does not have subdivisions, end the subregion flow
    if (!area_results.message.length) {
      return setCityAndArea(msg, token);
    }
  }
  return listPaginatedResource(
    msg,
    token,
    (offset) => getSubCity({ city: action.query.name, offset }),
    action,
    CALLBACK_API.SET_CITY_AND_AREA,
    newMsg
  );
}
