import { FindOrCreate } from '../../db/user';
import { DeleteBookMark, getAllBookmarks } from '../../db/bookmark';
import { CALLBACK_API, sendMessage } from './telegramBot';
import { FindResourcesByIds } from '../../db/resource';

const displayResource = (resource) => {
  const combined = resource.dataValues.PHONE_NUMBER.split(',').map(
    (number) => `Click to call: ${number}\n`
  );
  return `${resource.dataValues.NAME} in ${resource.dataValues.CITY}:\n<b>${resource.dataValues.VENDOR_NAME}</b>\n<i>${resource.dataValues.ADDITIONAL_COMMENTS}</i>\n${combined}`;
};

export async function listBookmarks(msg, token) {
  const callBackJson = {
    api: CALLBACK_API.DELETE_BOOKMARK,
  };

  const userTelegramId = msg.chat.id;
  try {
    const user = await FindOrCreate(userTelegramId);
    const bookmarks = await getAllBookmarks(user.id);
    const resourceIds = bookmarks.map((bookmark) => bookmark.dataValues.resourceId);
    const resources = await FindResourcesByIds(resourceIds);
    if (resources.length === 0) {
      return sendMessage(
        token,
        msg.chat.id,
        'You have not bookmarked any resources, click on /list to view or bookmark some now'
      );
    }
    resources.map((resource) => sendMessage(token, msg.chat.id, displayResource(resource), {
      parse_mode: 'HTML',
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: '❌ delete bookmark',
              callback_data: JSON.stringify({
                ...callBackJson,
                query: {
                  id: resource.id,
                  userId: user.id,
                },
              }),
            },
          ],
        ],
      },
    }));

    return {
      status: 200,
      message: 'All bookmarks sent',
    };
  } catch (error) {
    console.error('Something went wrong trying to list bookmarks: ', error);
    return {
      status: 500,
      message: 'List bookmarks returned error',
    };
  }
}

export async function deleteBookmark(msg, token, action) {
  const { id, userId } = action.query;
  try {
    await DeleteBookMark(userId, id);
    return sendMessage(
      token,
      msg.chat.id,
      'Successfully deleted bookmark, click on /bookmarks to see an updated list'
    );
  } catch (error) {
    console.error('Something went wrong trying to delete bookmark: ', error);
    return {
      status: error.status,
      message: 'Something went wrong trying to delete bookmark',
    };
  }
}
