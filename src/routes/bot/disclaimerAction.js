import { sendMessage, checkCity } from './telegramBot';
import { getUserCityAndArea } from '../../global/store';
import { listAllCities } from './listAllCityAction';

export function disclaimer(msg, token) {
  const disclaimer = `
      Disclaimer:
  •	All information, content, details, contact details, vendor details, views and all other details provided and expressed in this website are provided free of charge and have been collected and compiled online only to help and facilitate the viewers and covid patients during pandemic and as such the publisher does not endorse or verify them. As such in this pandemic it is not possible to verify the data and is subjected to errors and omissions. 

  •	The publisher assumes no responsibility whatsoever and the user is advised to check the authenticity before communicating and/or dealing with any of the vendors/suppliers. Take utmost care before communicating and/or dealing with any of the vendors/suppliers and use the data provided at your own risk.
  
  •	The publisher hereby inform that it has no nexus with any of the vendors/suppliers mentioned in the website.
  
  •	To improve the quality of data it is requested that the users may kindly notify the publisher at \/feedback regarding any wrong/incorrect information.
  
  •	User discretion to use the data is advised.`;

  return sendMessage(token, msg.chat.id, disclaimer).then(async (_) => {
    const isCitySet = await checkCity(msg, token);
    if (!isCitySet) {
      return listAllCities(msg, token);
    }

    const userTelegramId = msg.chat.id;
    const city = getUserCityAndArea(userTelegramId)
      ? getUserCityAndArea(userTelegramId).city
      : 'your city';
    const helpMsg = `Welcome to the info bot for covid resources around ${city}!
      Click /list to show a list of available resources in and around ${city}!
      Click /create to add information about a valid resource that you know.
      Click /feedback to send bugs or errors for the bot.
      
      Please do give us correct and accurate information. 🙏 let's help each other in these hard times.`;
    return sendMessage(token, msg.chat.id, helpMsg);
  });
}
