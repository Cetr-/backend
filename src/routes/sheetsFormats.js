export const SHEET_IDS = [
  // East India
  // https://docs.google.com/spreadsheets/d/17J71nPGmsor_xGNAxpWSk2AiCPm93RVgITeUe5cVa8c/edit#gid=0
  '17J71nPGmsor_xGNAxpWSk2AiCPm93RVgITeUe5cVa8c',
  // Central India
  // https://docs.google.com/spreadsheets/d/1OyPrcBvqaF4IUy3BbNQrrM7v-_h9oUfvN11ed3TcKKs/edit#gid=1578081011
  '1OyPrcBvqaF4IUy3BbNQrrM7v-_h9oUfvN11ed3TcKKs',
  // West india
  // https://docs.google.com/spreadsheets/d/1e95n1OnGD7aEYldbywqwJq-LNPUgoAjNuBETbNk_bkY/edit#gid=727711332
  '1e95n1OnGD7aEYldbywqwJq-LNPUgoAjNuBETbNk_bkY',
  // North
  // https://docs.google.com/spreadsheets/d/1nEF-UFOr71REFFKEwLrwxHt1Pd3sIpr33Q23VtxODN8/edit#gid=1533451059
  '1nEF-UFOr71REFFKEwLrwxHt1Pd3sIpr33Q23VtxODN8',
  // South
  // https://docs.google.com/spreadsheets/d/1ya7YddAhGOuTInFZywLfZ7Q2NnIt6K0mAHOdYEdbvPI/edit#gid=2029358203
  '1ya7YddAhGOuTInFZywLfZ7Q2NnIt6K0mAHOdYEdbvPI',
];

export const SUPPORTED_SHEET_TABS = [
  'Oxygen',
  'Hospitals',
  'Ambulance',
  'Pathology Labs',
  'Medicines & Pharmacy Related',
  'Plasma',
  'Food',
  'Home ICU',
  'Government Helpline / Important Contacts',
];

export const COLUMN_NAMES_TO_HEADERS = {
  NAME: ['NAME', 'Service'],
  SOURCE: ['SOURCE'],
  VENDOR_NAME: ['VENDOR_NAME', 'Name of Provider', 'Hospital Name'],
  VERIFIED: ['VERIFIED_BOOL'],
  ADDRESS: ['ADDRESS'],
  PHONE_NUMBER: ['PHONE NUMBER', 'Number', 'Contact'],
  AVAILABILITY_COUNT: ['AVAILABILITY COUNT'],
  ADDITIONAL_COMMENTS: ['ADDITIONAL_COMMENTS', 'Remarks / Comments', 'Remarks'],
  LAST_UPDATED: [
    'LAST UPDATED',
    'Time Stamp',
    'Verified as of(from external source )',
    'Verification Time Stamp',
    'Updated At (Time stamp for the latest status on the source)',
  ],
  CITY: ['CITY'],
  VERIFICATION_COUNT: ['Verified Count', 'VERIFICATION_COUNT'],
  L_AREA: ['STATE', 'State'],
  L_SUBAREA: ['CITY', 'District/Area/Locality', 'Location'],
  SUBTYPE: ['SUBTYPE', 'Product/Service', 'Hospital Type'],
  STATUS: ['Status', 'Available Y/N'],
  HOSPITAL_OXYGEN_BED: ['Oxygen Beds Available'],
  HOSPITAL_NON_OXYGEN_BED: ['Non-Oxygen Beds'],
  HOSPITAL_ICU: ['ICU'],
  HOSPITAL_VENTILATOR: ['Ventilator'],
};
