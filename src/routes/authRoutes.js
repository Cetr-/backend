import passport from 'passport';

const authRoutes = (indexRouter) => {
  indexRouter.get('/auth/google',
    passport.authenticate('google', {
      scope: [ 'profile', 'email' ]
    }));  

  indexRouter.get('/auth/google/callback', function(req, res, next) {
      passport.authenticate('google', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) {
          res.send('Access allowed to volunteers only. <a href="/">Click here to redirect</a>');
        }
        req.logIn(user, function(err) {
          if (err) { return next(err); }
          return res.redirect('/');
        });
      })(req, res, next);
    });

  indexRouter.get('/api/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });

  indexRouter.get('/api/current_user', (req, res) => {
    res.send(req.user);
  });
};

export default authRoutes;
