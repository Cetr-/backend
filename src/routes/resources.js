import { google } from 'googleapis';
import { findPhoneNumbersInText } from 'libphonenumber-js';
import {
  PhoneNumber,
  Resource,
  COLUMN_NAMES_TO_INDEX,
} from '../model/resources';
import {
  getCityAndState,
  findStateFromIsoCode,
  findCity,
  State,
  City,
} from '../model/stateAndCity';
import { GOOGLE_SHEETS_API_KEY, SPREAD_SHEET_ID } from '../settings';
import { COLUMN_NAMES_TO_HEADERS } from './sheetsFormats';
import { findAndInsertIfAbsent } from './database/findNumberAndInsert';
import { BulkCreate } from '../db/syncFromSheet';

function getValueForHeader(columns, header, row) {
  const i = header.findIndex((elem) =>
    columns
      .map((column) => column.trim().toLowerCase())
      .includes(elem.trim().toLowerCase())
  );

  if (i === -1) {
    return '';
  }
  return row[i] || '';
}

function isNotHeader(header, row) {
  const colNamesForPh = COLUMN_NAMES_TO_HEADERS.PHONE_NUMBER.map((column) =>
    column.trim().toLowerCase()
  );

  return !colNamesForPh.includes(
    getValueForHeader(COLUMN_NAMES_TO_HEADERS.PHONE_NUMBER, header, row)
      .trim()
      .toLowerCase()
  );
}

/**
 * Considers the row empty if there is no resource name.
 *
 * @param {[*]} row Array of sheet row
 * @returns
 */
function isNotEmpty(row) {
  return row[COLUMN_NAMES_TO_INDEX.NAME];
}

/*
 * Construct and get phone number object.
 */
function getPh(number) {
  return PhoneNumber({ input: number });
}

function getVerifiedStatus(header, row) {
  const status = getValueForHeader(COLUMN_NAMES_TO_HEADERS.STATUS, header, row);
  if (['TRUE', 'Verified'].includes(status)) {
    return 'VERIFIED_AVAILABLE';
  }

  return 'UNVERIFIED';
}

function isVerifiedOrNot(name, value) {
  if (['Yes'].includes(value)) {
    return `verified ${name}`;
  }
  if (['No'].includes(value)) {
    return `verified no ${name}`;
  }

  return `unverified ${name}`;
}
function getSubtype(header, row, name) {
  const subtype = getValueForHeader(
    COLUMN_NAMES_TO_HEADERS.SUBTYPE,
    header,
    row
  );
  if (name !== 'Hospitals') {
    return subtype;
  }

  const oxygenBeds = getValueForHeader(
    COLUMN_NAMES_TO_HEADERS.HOSPITAL_OXYGEN_BED,
    header,
    row
  );
  const nonOxygenBeds = getValueForHeader(
    COLUMN_NAMES_TO_HEADERS.HOSPITAL_NON_OXYGEN_BED,
    header,
    row
  );
  const icu = getValueForHeader(
    COLUMN_NAMES_TO_HEADERS.HOSPITAL_ICU,
    header,
    row
  );
  const ventilator = getValueForHeader(
    COLUMN_NAMES_TO_HEADERS.HOSPITAL_VENTILATOR,
    header,
    row
  );

  let subtypes = '';
  subtypes += isVerifiedOrNot('oxygen beds', oxygenBeds);
  subtypes += ',' + isVerifiedOrNot('non oxygen beds', nonOxygenBeds);
  subtypes += ',' + isVerifiedOrNot('icu', icu);
  subtypes += ',' + isVerifiedOrNot('ventilator', ventilator);

  return subtypes;
}

function convertRowToResource(header, row, name) {
  const cityAndState = getCityAndState(
    getValueForHeader(COLUMN_NAMES_TO_HEADERS.L_SUBAREA, header, row),
    getValueForHeader(COLUMN_NAMES_TO_HEADERS.L_AREA, header, row)
  );
  return new Resource({
    resourceName:
      getValueForHeader(COLUMN_NAMES_TO_HEADERS.NAME, header, row) || name,
    source: getValueForHeader(COLUMN_NAMES_TO_HEADERS.SOURCE, header, row),
    subType: getSubtype(header, row, name),
    vendorName: getValueForHeader(
      COLUMN_NAMES_TO_HEADERS.VENDOR_NAME,
      header,
      row
    ),
    address: getValueForHeader(COLUMN_NAMES_TO_HEADERS.ADDRESS, header, row),
    phoneNumbers: getPh(
      getValueForHeader(COLUMN_NAMES_TO_HEADERS.PHONE_NUMBER, header, row)
    ),
    availabilityCount: 'Unavailable',
    additionalComments: getValueForHeader(
      COLUMN_NAMES_TO_HEADERS.ADDITIONAL_COMMENTS,
      header,
      row
    ),
    verified:
      getValueForHeader(COLUMN_NAMES_TO_HEADERS.VERIFIED, header, row) ===
      'TRUE',
    L_SUBAREA: cityAndState.city,
    L_AREA: cityAndState.state,
    city: getValueForHeader(COLUMN_NAMES_TO_HEADERS.L_SUBAREA, header, row),
    status: getVerifiedStatus(header, row),
    lastUpdated:
      getValueForHeader(COLUMN_NAMES_TO_HEADERS.LAST_UPDATED, header, row) ||
      '',
  });
}

function splitRowByNumbers(row, header) {
  const phNumberVals =
    getValueForHeader(COLUMN_NAMES_TO_HEADERS.PHONE_NUMBER, header, row) || '';
  let listOfNumbers = findPhoneNumbersInText(phNumberVals, 'IN');
  if (phNumberVals.includes(',')) {
    listOfNumbers = phNumberVals
      .split(',')
      .flatMap((number) => findPhoneNumbersInText(number, 'IN'));
  }
  if (phNumberVals.includes('/')) {
    listOfNumbers = phNumberVals
      .split('/')
      .flatMap((number) => findPhoneNumbersInText(number, 'IN'));
  }
  if (phNumberVals.includes('\\')) {
    listOfNumbers = phNumberVals
      .split('\\')
      .flatMap((number) => findPhoneNumbersInText(number, 'IN'));
  }

  if (listOfNumbers.length >= 1) {
    const new_rows = listOfNumbers.map((phoneNumber, i) => {
      let newRow = [...row];
      const ph_idx = header.findIndex((elem) =>
        COLUMN_NAMES_TO_HEADERS.PHONE_NUMBER.map((column) =>
          column.trim().toLowerCase()
        ).includes(elem.trim().toLowerCase())
      );
      const name_idx = header.findIndex((elem) =>
        COLUMN_NAMES_TO_HEADERS.VENDOR_NAME.map((column) =>
          column.trim().toLowerCase()
        ).includes(elem.trim().toLowerCase())
      );
      newRow[parseInt(name_idx)] = newRow[name_idx] + (i == 0 ? '' : ` ${i}`);
      newRow[parseInt(ph_idx)] = phoneNumber.number.number;

      return newRow;
    });

    return new_rows;
  }

  return [row];
}

function getErrorCode(errorMsg) {
  if (errorMsg.includes('vendorName')) {
    return 'BLANK_VENDOR_NAME';
  }
  if (errorMsg.includes('isValidPhoneNumber')) {
    return 'INCORRECT_PHONE_NUMBER';
  }
  if (errorMsg.includes('isAnIndianCity')) {
    return 'INCORRECT_CITY_STATE';
  }
  if (errorMsg.includes('checkIsIndianState')) {
    return 'INCORRECT_STATE';
  }

  return 'INCORRECT_FORMAT';
}

/**
 * Converts the results returned from google api into resource object.
 *
 * @param {*} results From calling the google sheets api.
 * @returns {[Resource]} list of resource objects
 */
async function insertResultsToResource(results, name, sheetId) {
  const rows = results.data.values;
  const listOfValidResources = [];
  const listOfInvalidResources = [];
  const header = rows[0]; // Assumes header is always present
  let total_processed = 0;
  let total_inserted = 0;
  let total_updated = 0;
  let total_errors = 0;
  if (rows.length) {
    const splitRows = rows
      .filter((row) => isNotHeader(header, row))
      .flatMap((row) => splitRowByNumbers(row, header));

    for (const index in splitRows) {
      const row = splitRows[index];
      if (isNotEmpty(row)) {
        total_processed += 1;
        try {
          const resource = convertRowToResource(header, row, name);
          const insertRes = await findAndInsertIfAbsent(resource);
          if (insertRes.status === 'INSERTED') {
            total_inserted += 1;
            listOfValidResources.push(insertRes);
          } else if (insertRes.status === 'UPDATED') {
            total_updated += 1;
            listOfValidResources.push(insertRes);
          } else {
            total_errors += 1;
            listOfInvalidResources.push(
              Object.assign(insertRes, {
                row,
                sheetId,
              })
            );
          }
        } catch (e) {
          total_errors += 1;
          if (e instanceof TypeError) {
            listOfInvalidResources.push({
              status: 'NOT_INSERTED',
              row,
              error: getErrorCode(e.message),
              message: e.message,
              sheetId,
            });
          } else {
            listOfInvalidResources.push({
              status: 'NOT_INSERTED',
              row,
              error: 'UNKNOWN_ERROR',
              message: e.message,
              sheetId,
            });
          }
        }
      }
    }
  }

  console.log('done parsing rows');
  BulkCreate(listOfInvalidResources, sheetId, {
    total_processed,
    total_inserted,
    total_errors,
    total_updated,
  });

  return {
    total_processed,
    total_inserted,
    total_updated,
    total_errors,
    error_records: listOfInvalidResources,
    inserted_records: listOfValidResources,
  };
}

/**
 * Fetches resources from the primary google sheet specified by name.
 * This name should match the tab name.
 *
 * @param {String} name Name of the google sheet tab
 * @returns
 */
async function fetchResourcesAndInsert(name, sheetId = SPREAD_SHEET_ID) {
  const sheets = google.sheets({ version: 'v4', auth: GOOGLE_SHEETS_API_KEY });
  try {
    const result = await sheets.spreadsheets.values.get({
      spreadsheetId: sheetId,
      range: name,
    });
    return insertResultsToResource(result, name, sheetId);
  } catch (e) {
    if (e.code === 400) {
      return {
        error_records: [],
        inserted_records: [],
      };
    }
    return 'There was an error in fetching your data. Please contact developer';
  }
}

function convertResultToResource(results) {
  const rows = results.data.values;
  const listOfValidResources = [];
  const header = rows[0]; // Assumes header is always present
  let total_processed = 0;
  let total_errors = 0;
  if (rows.length) {
    const splitRows = rows
      .filter((row) => isNotHeader(header, row))
      .flatMap((row) => splitRowByNumbers(row, header));

    for (const index in splitRows) {
      const row = splitRows[index];
      if (isNotEmpty(row)) {
        total_processed += 1;
        try {
          const resource = convertRowToResource(header, row);
          listOfValidResources.push(resource);
        } catch (e) {
          total_errors += 1;
        }
      }
    }
  }

  return listOfValidResources;
}

/**
 * Fetches resources from the primary google sheet specified by name.
 * This name should match the tab name.
 *
 * @param {String} name Name of the google sheet tab
 * @returns
 */
async function fetchResourcesFromSheet(name) {
  const sheets = google.sheets({ version: 'v4', auth: GOOGLE_SHEETS_API_KEY });
  try {
    const result = await sheets.spreadsheets.values.get({
      spreadsheetId: SPREAD_SHEET_ID,
      range: name,
    });
    return convertResultToResource(result);
  } catch (e) {
    return 'There was an error in fetching your data. Please contact developer';
  }
}

/**
 * Converts an app request to a resource object.
 *
 * @param {*} request
 * @returns
 */
function convertRequestToResource(request) {
  if (!request.name) {
    throw { status: 400, message: 'Request must have a name' };
  }

  if (!request.city) {
    throw { status: 400, message: 'Request must have a city' };
  }

  if (!request.phoneNumbers) {
    throw { status: 400, message: 'Request must have a number' };
  }

  const state = getStateFromCity(request.city);

  return new Resource({
    resourceName: request.name,
    vendorName: request.vendorName,
    address: request.address,
    phoneNumbers: PhoneNumber({ input: request.phoneNumbers[0] }),
    availabilityCount: request.availabilityCount || '-1',
    additionalComments: request.additionalComments || '',
    verified: request.verified,
    lastUpdated: new Date(),
    L_AREA: State({ state: state.name }),
    L_SUBAREA: City({ state: state.name, city: request.city }),
    city: request.city,
  });
}

function getStateFromCity(cityName) {
  const city = findCity(cityName, '');
  return findStateFromIsoCode(city.stateCode);
}

export {
  fetchResourcesAndInsert,
  convertRequestToResource,
  fetchResourcesFromSheet,
};
