import express from 'express';
import { findResourcesByTypeAndLocation } from '../database/findResources';
import { updateResourcesFromWeb } from './UpdateResourcesAction';
import requireLogin from '../../middleware/requireLogin';
import { getResourceTypes } from '../bot/database/getResourceTypes';
import { getAllCities, getSubCity } from '../database/GetDistinctCityAndState';

const websiteRouter = express.Router();

// Fetch resources based on the given filters
websiteRouter.get('/resources', async (req, res) => {
  if (!req.query.resourceName) {
    return res.status(400).json({
      message:
        'BAD_REQUEST please add a resource name in query e.g. ..?resourceName=Oxygen',
    });
  }

  if (!req.query.state) {
    return res.status(400).json({
      message:
        'BAD_REQUEST please add a state in your query e.g. ..?state=Uttar Pradesh',
    });
  }

  const resReq = {
    type: req.query.resourceName,
    L_AREA: req.query.state,
  };

  if (req.query.city && req.query.city.trim().toLowerCase() !== 'all') {
    resReq.L_SUBAREA = req.query.city;
  }

  const result = await findResourcesByTypeAndLocation(resReq);
  return res.status(200).json(result);
});

websiteRouter.get('/states', async (req, res) => {
  const response = await getAllCities();
  return res.status(response.status).json({ states: response.message || [] });
});

websiteRouter.get('/cities', async (req, res) => {
  if (!req.query.state) {
    return res.status(400).json({
      message: 'BAD_REQUEST Atleast a state name should be present',
    });
  }

  const response = await getSubCity({
    city: req.query.state,
  });
  return res.status(response.status).json({ cities: response.message || [] });
});

websiteRouter.get('/types', async (req, res) => {
  if (!req.query.state) {
    return res.status(400).json({
      message:
        'BAD_REQUEST Atleast a state name should be present. City name is optional.',
    });
  }

  const resReq = { city: req.query.state, L_AREA: req.query.state };

  if (req.query.city && req.query.city.trim().toLowerCase() !== 'all') {
    resReq.L_SUBAREA = req.query.city;
  }

  const response = await getResourceTypes(resReq);
  return res.status(response.status).json({ types: response.message || [] });
});

// Update existing resources
websiteRouter.put('/resources/:id', requireLogin, async (req, res) => {
  if (req.body.STATUS) {
    if (
      ![
        'UNVERIFIED',
        'VERIFIED_AVAILABLE',
        'VERIFIED_RECHECK_AVAILABILITY',
        'VERIFIED_UNAVAILABLE',
        'NOT_REACHABLE',
        'FAKE',
      ].includes(req.body.STATUS)
    ) {
      return res.status(400).json({
        message: 'BAD_REQUEST bad value of status is set',
      });
    }
  }

  const result = await updateResourcesFromWeb(req.params.id, req.body);

  return res.status(result.status).json(result);
});

export default websiteRouter;
