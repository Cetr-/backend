import { UpdateOldValues, FindResourcesByIds } from '../../db/resource';

export async function updateResourcesFromWeb(id, request) {
  const newUpdatedFields = {};
  if (request.ADDITIONAL_COMMENTS) {
    newUpdatedFields.ADDITIONAL_COMMENTS = request.ADDITIONAL_COMMENTS;
  }
  if (request.CHECK_AFTER) {
    newUpdatedFields.CHECK_AFTER = request.CHECK_AFTER;
  }
  if (request.VERIFICATION_COUNT) {
    newUpdatedFields.VERIFICATION_COUNT = request.VERIFICATION_COUNT;
  }
  if (request.STATUS) {
    newUpdatedFields.STATUS = request.STATUS;
  }

  if (request.LAST_UPDATED) {
    newUpdatedFields.LAST_UPDATED = request.LAST_UPDATED;
  }

  if (!Object.keys(newUpdatedFields).length) {
    return {
      status: 200,
      resource: {},
      message: 'There was nothing new to change!',
    };
  }

  const result = await UpdateOldValues(id, newUpdatedFields);

  if (result.status === 201) {
    const newResource = await FindResourcesByIds([ id ]);
    return {
      status: 200,
      resource: newResource,
      message: 'Updated resource',
    };
  }

  return {
    status: 400,
    resource: {},
    message: 'Unable to update resource',
  };
}
