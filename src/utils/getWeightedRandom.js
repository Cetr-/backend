import rwc from 'random-weighted-choice';
import { getRandomInt } from './randomUtils';

// db's asc sort function puts null values at the start of the list
function getLeastWeight(resources) {
  for (let i = 0; i < resources.length; i += 1) {
    if (resources[i].VERIFICATION_COUNT !== null) {
      return resources[i].VERIFICATION_COUNT;
    }
  }
  return 0;
}

/**
 *
 * @param resources - list of resources sorted by VERIFICATION_COUNT (ascending) by db
 * @returns resource - a resource randomly selected from input - weighted by VERIFICATION_COUNT
 */
export const getWeightedRandom = (resources) => {
  const leastWeight = getLeastWeight(resources);
  const offset = leastWeight <= 0 ? Math.abs(leastWeight) + 1 : 1;
  const weightedIndices = resources.map((resource, index) => ({
    weight: (resource.VERIFICATION_COUNT || 0) + offset,
    id: index,
  }));
  const randomIndex = rwc(weightedIndices, 0);
  if (randomIndex) {
    return resources[randomIndex];
  }
  return resources[getRandomInt(resources.length)];
};
