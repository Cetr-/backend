import { ObjectModel } from 'objectmodel';
import csc from 'country-state-city';

const COMBINING = /[\u0300-\u036F]/g;

function findState(name) {
  return csc
    .getStatesOfCountry('IN')
    .find((elem) =>
      elem.name.trim().toLowerCase().includes(name.trim().toLowerCase())
    );
}

export function findStateFromIsoCode(isoCode) {
  return csc.getStatesOfCountry('IN').find((elem) => elem.isoCode === isoCode);
}

export function findCity(cityName, state = '') {
  if (!state) {
    return csc
      .getCitiesOfCountry('IN')
      .find((city) =>
        city.name
          .normalize('NFKD')
          .replace(COMBINING, '')
          .trim()
          .toLowerCase()
          .includes(cityName.trim().toLowerCase())
      );
  }
  return csc
    .getCitiesOfState('IN', state.isoCode)
    .find((city) =>
      city.name
        .trim()
        .normalize('NFKD')
        .replace(COMBINING, '')
        .toLowerCase()
        .includes(cityName.trim().toLowerCase())
    );
}

function checkIsIndianState(name) {
  // Allow exception for Delhi.
  if (name.trim().toLowerCase().includes('delhi')) {
    return true;
  }
  const state = findState(name);
  if (state) {
    return true;
  }
  return false;
}

function formatStateName(name) {
  if (name.trim().toLowerCase().includes('delhi')) {
    return 'delhi - ncr';
  }
  const state = findState(name);

  if (state.isoCode === 'DL') {
    return 'delhi - ncr';
  }
  return state.name.trim().toLowerCase();
}

export const State = ObjectModel({
  state: String,
})
  .assert((obj) => checkIsIndianState(obj.state))
  .defaultTo({
    getFormatted() {
      return formatStateName(this.state);
    },
  })
  .as('Valid state name');

export const City = ObjectModel({
  city: String,
  state: String,
})
  .assert((str) => {
    if (str.state.trim().toLowerCase().includes('delhi')) {
      return true;
    }

    const state = findState(str.state);
    const city = findCity(str.city, state);
    if (!city) {
      return false;
    }

    if (state.isoCode !== city.stateCode) {
      return false;
    }

    return true;
  })
  .defaultTo({
    getFormatted() {
      if (this.state.trim().toLowerCase().includes('delhi')) {
        return this.city || 'all';
      }
      const city = findCity(this.city);
      return city.name.trim().toLowerCase();
    },
  })
  .as('Valid city name');

export function getCityAndState(cityName, stateName) {
  if (!stateName.trim()) {
    // default to given city and state name
    const city = findCity(cityName) || { name: cityName };
    const state = findStateFromIsoCode(city.stateCode) || { name: stateName };
    return {
      state: State({ state: state.name }),
      city: City({ city: city.name, state: state.name }),
    };
  }

  return {
    state: State({ state: stateName }),
    city: City({ city: cityName, state: stateName }),
  };
}
