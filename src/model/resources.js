import { ObjectModel, BasicModel } from 'objectmodel';
import { isValidPhoneNumber, parsePhoneNumber } from 'libphonenumber-js';
import { City, State } from './stateAndCity';

export const PhoneNumber = ObjectModel({
  input: String,
})
  .assert((number) => isValidPhoneNumber(number.input, 'IN'))
  .defaultTo({
    getFormatted() {
      const ph = parsePhoneNumber(this.input, 'IN');
      return ph.formatInternational();
    },
  })
  .as('Valid number');

export const StringNotBlank = BasicModel(String)
  .assert((str) => str.trim().length > 0)
  .as('StringNotBlank');

/**
 * Defines a reource type and its parameters.
 */
export const Resource = new ObjectModel({
  // name of resource e.g. Oxygen
  resourceName: String,
  // Optional. any subtype of resource
  subType: [String],
  // From where this data was gathered.
  source: [String],
  // Optional. name of merchant/person/hospital providing the resource
  vendorName: StringNotBlank,
  // Mark whether this information is verified or not.
  verified: Boolean,
  // List of phone numbers for the resource
  phoneNumbers: PhoneNumber,
  // Total amount/availability of this resource.
  availabilityCount: [String, 'Unavailable'],
  // Additional comments e.g. opening time/cost etc.
  additionalComments: [String],
  // Timestamp on when information regarding this resource was last updated.
  lastUpdated: [String],
  // City
  city: String,
  // Optional. address of the vendor
  address: [String],
  // Bigger region of this resource like state
  L_AREA: State,
  // Smaller region of this resource like city
  L_SUBAREA: City,
  // Status enum
  status: [
    'UNVERIFIED',
    'VERIFIED_AVAILABLE',
    'VERIFIED_RECHECK_AVAILABILITY',
    'VERIFIED_UNAVAILABLE',
    'NOT_REACHABLE',
    'FAKE',
  ],
}).defaultTo({
  verified: false,
  availabilityCount: 'Unavailable',
});

const NAME = 'Name';
const SOURCE = 'Source';
const VENDOR_NAME = 'Vendor name';
const VERIFIED = 'Verified';
const ADDRESS = 'Address';
const PHONE_NUMBER = 'Phone number';
const AVAILABILITY_COUNT = 'Availability count';
const ADDITIONAL_COMMENTS = 'Additional comments';
const LAST_UPDATED = 'Last updated';
const CITY = 'City';
const VERIFICATION_COUNT = 'VERIFICATION_COUNT';
const L_AREA = 'L_AREA';
const L_SUBAREA = 'L_SUBAREA';
const SUBTYPE = 'SUBTYPE';

export const COLUMN_NAMES_TO_INDEX = {
  NAME: 0,
  SOURCE: 1,
  VENDOR_NAME: 2,
  VERIFIED: 3,
  ADDRESS: 4,
  PHONE_NUMBER: 5,
  AVAILABILITY_COUNT: 6,
  ADDITIONAL_COMMENTS: 7,
  LAST_UPDATED: 8,
};

export const COLUMN_NAMES_TO_HEADER = {
  NAME: 'NAME',
  SOURCE: 'SOURCE',
  VENDOR_NAME: 'VENDOR_NAME',
  VERIFIED: 'VERIFIED_BOOL',
  ADDRESS: 'ADDRESS',
  PHONE_NUMBER: 'PHONE NUMBER',
  AVAILABILITY_COUNT: 'AVAILABILITY COUNT',
  ADDITIONAL_COMMENTS: 'ADDITIONAL_COMMENTS',
  LAST_UPDATED: 'LAST UPDATED',
  CITY: 'CITY',
  VERIFICATION_COUNT,
  L_AREA: 'STATE',
  L_SUBAREA: 'CITY',
  SUBTYPE: 'SUBTYPE',
};

export const COLUMNS = {
  NAME,
  SOURCE,
  VENDOR_NAME,
  VERIFIED,
  ADDRESS,
  PHONE_NUMBER,
  AVAILABILITY_COUNT,
  ADDITIONAL_COMMENTS,
  LAST_UPDATED,
  CITY,
  VERIFICATION_COUNT,
  L_AREA,
  L_SUBAREA,
  SUBTYPE,
};
