import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
import { GOOGLE_OAUTH_CLIENT_ID, GOOGLE_OAUTH_CLIENT_SECRET } from '../settings';
import {
  createWebUser, findWebUserOrCreate, findWebUserByDbId, findWebUserByGoogleId
} from '../db/webUser';
import { findVolunteerUserByEmail } from '../db/volunteerUser';

passport.serializeUser((user, done) => {
  // null stands for an error object
  // user.id is an identifier for a user to be set to a cookie
  // this id is not the googleId, but id returned from database
  done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
  const user = await findWebUserByDbId(id);
  done(null, user);
});

passport.use(
  new GoogleStrategy.Strategy(
	  {
      clientID: GOOGLE_OAUTH_CLIENT_ID,
      clientSecret: GOOGLE_OAUTH_CLIENT_SECRET,
      callbackURL: '/v1/auth/google/callback',
	  }, async (accessToken, refreshToken, profile, done) => {
      // Check if user is in the list of volunteers
      const volunteerUser = await findVolunteerUserByEmail(profile.emails[0].value);
      if (volunteerUser) {
		    console.log('found user');
        const { id } = profile;
        const user = await findWebUserByGoogleId(id);
        if (user) {
          console.log('User already exists: ', id);
          done(null, user);
        } else {
          const newUser = await createWebUser(id, profile.emails[0].value);
          console.log('User does not exist, created now: ', id);
          done(null, newUser);
        }
      } else {
		    done(null, false, 'Access allowed to volunteers only');
	    }
      // return user;
	  }
  )
);
export default passport;
