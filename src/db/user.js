import { Sequelize } from 'sequelize';
import { sequelize } from '../server';

export const User = sequelize.define(
  'users',
  // User's unique id from telegram api
  { telegramId: Sequelize.STRING },
  { indexes: [ { unique: true, allowNull: false, fields: [ 'telegramId' ] } ] }
);

export const FindOrCreate = async (userId) => {
  try {
    const userValues = await User.findOrCreate({
      where: { telegramId: userId },
    });
    return userValues[0].dataValues;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to create/find user: ', e);
    throw e;
  }
};
