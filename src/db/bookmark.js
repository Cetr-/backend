import { sequelize } from '../server';
import { User } from './user';
import { Resource } from './resource';

/** Bookmarks model */
export const Bookmarks = sequelize.define('bookmarks');

User.belongsToMany(Resource, { through: Bookmarks, foreignKey: 'userId' });
Resource.belongsToMany(User, { through: Bookmarks, foreignKey: 'resourceId' });

/** Create a bookmark */
export const CreateBookmark = async (userId, resourceId) => {
  try {
    await Bookmarks.create({ userId, resourceId });
    return {
      status: 201,
      message: 'Successfully bookmarked',
    };
  } catch (error) {
    console.error('Could not bookmark: ', error);
    return {
      status: 500,
      message: 'Error on bookmark delete',
    };
  }
};

/**
 * Finds bookmark by userId and resourceId
 * @param userId
 * @param resourceId
 * @returns {Promise<null|Model<any, Bookmarks>>}
 * @constructor
 */
export const FindBookmark = async (userId, resourceId) => {
  try {
    return await Bookmarks.findOne({ where: { userId, resourceId } });
  } catch (error) {
    console.info('Could not find bookmark ', error);
    return null;
  }
};

/** Delete a bookmark */
export const DeleteBookMark = async (userId, resourceId) => {
  try {
    await Bookmarks.destroy({ where: { userId, resourceId } });
    return {
      status: 201,
      message: 'Successfully deleted bookmark',
    };
  } catch (error) {
    error.status = 500;
    throw error;
  }
};

/** Get all bookmark for given user */
export const getAllBookmarks = async (userId) => {
  try {
    return await Bookmarks.findAll({
      where: { userId },
    });
  } catch (error) {
    console.error('Couldn\'t find bookmark for: ', userId, error);
    return {
      status: 500,
      message: 'Internal db error on fetch',
    };
  }
};
