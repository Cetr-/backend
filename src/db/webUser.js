import { Sequelize } from 'sequelize';
import { sequelize } from '../server';

export const WebUser = sequelize.define(
  'webUsers',
  // User's unique id from Google API
  { googleId: Sequelize.STRING, email: Sequelize.STRING },
  { indexes: [ { unique: true, allowNull: false, fields: [ 'googleId' ] } ] }
);

export const createWebUser = async (userGoogleId, email) => {
  try {
    const newUser = await WebUser.create({ googleId: userGoogleId, email: email });
    console.log('Saved: ', newUser.googleId); // newUser exists in the database now!
    return newUser;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to create WebUser: ', e);
    throw e;
  }
};

export const findWebUserByGoogleId = async (userGoogleId) => {
  try {
    const user = await WebUser.findOne({
      where: { googleId: userGoogleId }
    });
    return user;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to find WebUser by googleId: ', e);
    throw e;
  }
};

export const findWebUserByDbId = async (dbId) => {
  try {
    const user = await WebUser.findOne({
      where: { id: dbId }
    });
    return user;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to find WebUser by dbId: ', e);
    throw e;
  }
};
