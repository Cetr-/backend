import { Op, Sequelize } from 'sequelize';
import { sequelize } from '../server';

export const Resource = sequelize.define('resources', {
  NAME: Sequelize.STRING,
  SOURCE: Sequelize.STRING,
  VENDOR_NAME: Sequelize.STRING,
  VERIFIED: Sequelize.BOOLEAN,
  ADDRESS: Sequelize.STRING,
  PHONE_NUMBER: Sequelize.STRING,
  AVAILABILITY_COUNT: Sequelize.INTEGER,
  ADDITIONAL_COMMENTS: Sequelize.STRING,
  LAST_UPDATED: Sequelize.STRING,
  CITY: Sequelize.STRING,
  VERIFICATION_COUNT: Sequelize.INTEGER,
  L_AREA: Sequelize.STRING,
  L_SUBAREA: Sequelize.STRING,
  SUBTYPE: Sequelize.STRING,
  STATUS: Sequelize.ENUM(
    'UNVERIFIED',
    'VERIFIED_AVAILABLE',
    'VERIFIED_RECHECK_AVAILABILITY',
    'VERIFIED_UNAVAILABLE',
    'NOT_REACHABLE',
    'FAKE'
  ),
  CHECK_AFTER: Sequelize.DATE,
});

/**
 * Creates a db record with the given resource.
 *
 * @param {Resource} resource model
 * @returns
 */
export const Create = async (resource) =>
  Resource.create({
    NAME: resource.resourceName,
    VENDOR_NAME: resource.vendorName,
    VERIFIED: resource.verified === 'Undefined' ? false : resource.verified,
    ADDRESS: resource.address,
    PHONE_NUMBER: resource.phoneNumbers.getFormatted(),
    ADDITIONAL_COMMENTS: resource.additionalComments,
    LAST_UPDATED: resource.lastUpdated,
    CITY: resource.city.trim().toLowerCase(),
    L_AREA: resource.L_AREA.getFormatted(),
    L_SUBAREA: resource.L_SUBAREA.getFormatted(),
    SUBTYPE: resource.subType,
    SOURCE: resource.source,
    STATUS: resource.status,
  })
    .then((data) => ({
      status: 200,
      message: data,
    }))
    .catch((_) => ({
      status: 500,
      message: 'Error in creating resource' + _,
    }));

/**
 * Updates given db record with values
 *
 * @param {Resource} resource model
 * @returns
 */
export const Update = async (id, verificationCount) =>
  Resource.findByPk(id).then((data) => {
    const updatedResource = {
      ...data.dataValues,
      VERIFICATION_COUNT: data.dataValues.VERIFICATION_COUNT
        ? data.dataValues.VERIFICATION_COUNT + verificationCount
        : verificationCount,
    };

    return Resource.update(updatedResource, {
      where: { id },
    })
      .then((num) => {
        if (num == 1) {
          return { status: 201, message: 'Success' };
        }

        return { status: 400, message: 'No record found to update' };
      })
      .catch((_) => ({ status: 500, message: 'Error in updating recors' }));
  });

export const FindRecordByNumber = async (number) => {
  const results = await Resource.findAll({
    where: {
      PHONE_NUMBER: number,
    },
  });

  if (results.length === 0) {
    return [];
  }

  return results;
};

export const FindResourcesByIds = async (resourceIds) =>
  await Resource.findAll({
    where: {
      id: {
        [Op.in]: resourceIds,
      },
    },
  });

/**
 * Updates given db record with values
 *
 * @returns
 */
export const UpdateOldValues = async (id, newValues) =>
  Resource.findByPk(id).then((data) => {
    const updateFields = {};
    if (newValues.L_AREA) {
      updateFields.L_AREA = newValues.L_AREA;
    }
    if (newValues.L_SUBAREA) {
      updateFields.L_SUBAREA = newValues.L_SUBAREA;
    }
    if (newValues.ADDITIONAL_COMMENTS) {
      updateFields.ADDITIONAL_COMMENTS = newValues.ADDITIONAL_COMMENTS;
    }
    if (newValues.SUBTYPE) {
      updateFields.SUBTYPE = newValues.SUBTYPE;
    }
    if (newValues.LAST_UPDATED) {
      updateFields.LAST_UPDATED = newValues.LAST_UPDATED;
    }
    if (newValues.CHECK_AFTER) {
      updateFields.CHECK_AFTER = newValues.CHECK_AFTER;
    }
    if (newValues.VERIFICATION_COUNT) {
      updateFields.VERIFICATION_COUNT = data.dataValues.VERIFICATION_COUNT
        ? data.dataValues.VERIFICATION_COUNT + newValues.VERIFICATION_COUNT
        : newValues.VERIFICATION_COUNT;
    }

    const updatedResource = {
      ...data.dataValues,
      ...updateFields,
    };

    return Resource.update(updatedResource, {
      where: { id },
    }).then((num) => {
      if (num === 1 || num[0] === 1) {
        return { status: 201, message: 'Success' };
      }
      return { status: 400, message: 'No record found to update' };
    });
    // .catch((_) => ({ status: 500, message: 'Error in updating recors' }));
  });
