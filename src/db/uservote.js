import { DataTypes } from 'sequelize';
import { sequelize } from '../server';
import { User } from './user';
import { Resource } from './resource';

export const UserVote = sequelize.define('uservotes', {
  vote: DataTypes.BOOLEAN,
});

User.belongsToMany(Resource, { through: UserVote, foreignKey: 'userId' });
Resource.belongsToMany(User, { through: UserVote, foreignKey: 'resourceId' });

export const Find = async (userId, resourceId) => {
  try {
    const userVote = await UserVote.findOne({
      where: { userId, resourceId },
    });
    if (userVote) {
      return userVote.dataValues;
    }
    return null;
  } catch (e) {
    console.error('Something went wrong retrieving uservote', error);
    return null;
  }
};

export const UpdateVote = async (userId, resourceId, vote) => {
  try {
    await UserVote.update(
      { vote },
      {
        where: { userId, resourceId },
      }
    );
    return { status: 200, message: 'Success' };
  } catch (error) {
    console.error('Could not update vote: ', error);
    error.status = 500;
    throw error;
  }
};

export const CreateVote = async (userId, resourceId, vote) => {
  try {
    await UserVote.create({ userId, resourceId, vote });
    return {
      status: 201,
      message: 'Successfully created user vote',
    };
  } catch (error) {
    console.error('Could not create vote: ', error);
    error.status = 500;
    throw error;
  }
};
