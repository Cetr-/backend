import { Sequelize } from 'sequelize';
import { sequelize } from '../server';

export const SyncErrors = sequelize.define('syncErrors', {
  ERROR: Sequelize.STRING,
  STATUS: Sequelize.STRING,
  ORIGINAL_ROW: Sequelize.STRING,
  MESSAGE: Sequelize.STRING,
  SHEET_ID: Sequelize.STRING,
  SUMMARY: Sequelize.STRING,
});

/**
 * Record sync errors
 * @param {errRows} json
 * @returns
 */
export const BulkCreate = async (errRows, sheetId, summary) =>
  SyncErrors.bulkCreate(
    errRows.map((input) => ({
      ERROR: input.error,
      STATUS: input.status,
      ORIGINAL_ROW: input.row.join(','),
      MESSAGE: JSON.stringify(input.message).substring(0, 255),
      SHEET_ID: sheetId,
      SUMMARY: JSON.stringify(summary),
    }))
  );
