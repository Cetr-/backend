import { DataTypes, Sequelize } from 'sequelize';
import { sequelize } from '../server';

export const VolunteerUser = sequelize.define('VolunteerUser', {
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  }
});

export const createVolunteerUser = async (email) => {
  try {
    const volunteerUser = await VolunteerUser.create({ email });
    console.log('Saved: ', volunteerUser.email);
    return volunteerUser;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to create VolunteerUser: ', e);
    throw e;
  }
};

export const findVolunteerUserByEmail = async (email) => {
  try {
    const volunteerUser = await VolunteerUser.findOne({
      where: { email }
    });
    return volunteerUser;
  } catch (e) {
    e.status = 500;
    console.error('Something went wrong trying to create VolunteerUser: ', e);
  }
};
