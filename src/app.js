import logger from 'morgan';
import flash from 'req-flash';
import express from 'express';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import passport from 'passport';
import indexRouter from './routes/index';
import botRouter from './routes/bot/routes';
import websiteRouter from './routes/website/index';
import { sequelize } from './server';
import { COOKIE_KEY } from './settings';

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(
  cookieSession({
    maxAge: 30 * 24 * 60 * 60 * 1000,
    keys: [COOKIE_KEY],
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use('/v1', indexRouter);
app.use('/bot', botRouter);
app.use('/web', websiteRouter);

sequelize
  .authenticate()
  /*
     Maximum keys allowed = 64 - more info - https://github.com/sequelize/sequelize/issues/9653
     Only sync when db is changed!!
   */
  // .then(() => {
  //   console.log('Database Connected!');
  //   return sequelize.sync({ alter: true, force: false });
  // })
  .then(() => {
    console.log('Database Migrated!');
  });
export default app;
