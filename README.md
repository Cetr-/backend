# One time set-up

1. Install node > v14
2. Install npm/yarn
3. git clone `git clone https://Cetr-@bitbucket.org/Cetr-/backend.git`
4. Run `cd backend`
5. Run `npm install .` or `yarn install `
6. Create a file called .env and paste the following inside:

```
SPREADSHEETS_ID="1xMisFD8Ct0c5Fe9d42MR67uYEkvOl4_UA4iWPqsqS58"
GOOGLE_SHEETS_API_KEY=<Get an API KEY>
DB_URI=mysql://<USER>:<PASSWORD>@34.126.112.131:3306/covid_resources
TELE_BOT_API_TOKEN_LUCKNOW=<create a dev bot using botFather on telegram>
TELE_BOT_API_TOKEN_DELHI=... (Other city bots can be added here)
GOOGLE_OAUTH_CLIENT_ID=<>
GOOGLE_OAUTH_CLIENT_SECRET=<>
COOKIE_KEY=12jhashiopoquqwwnnqw27218191oyqmncasp12
```

The spreadsheet id belongs to [a test sheet](https://docs.google.com/spreadsheets/d/1xMisFD8Ct0c5Fe9d42MR67uYEkvOl4_UA4iWPqsqS58/edit#gid=00)

7. Run `` yarn startdev`

# Tests

1. Run `yarn test` or `npm run test`

# Lint

Before commiting, lint to format your files `yarn postpretty`
