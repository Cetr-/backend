import { google } from 'googleapis';
import sinon from 'sinon';
import { Resource } from '../src/model/resources';
import { expect, server, RESOUCE_URL } from './setup';

function createRowHeader() {
  return [ 'Name', 'Source', 'Vendor name', 'Verified', 'Address',
    'Phone number', 'Availability count', 'Additional comments', 'Last updated' ];
}

function createValidResourceRow(resourceName) {
  return [ resourceName, 'Source', 'Merhcant', 'TRUE', 'Address',
    '+000,9929', '20', 'Additional comments', '' ];
}

function createInValidResourceRow() {
  return [ '', '', '', 'TRUE', 'Address',
    '+000,9929', '20', 'Additional comments', '4/21/2021' ];
}

describe('Resouce page test', () => {
  let stubl;

  it('throws error if resourceName is not set', done => {
    server
      .get(`${RESOUCE_URL}`)
      .expect(400)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.message).to.equal(
          'BAD_REQUEST please add a resource name in query e.g. ..?resourceName=Oxygen'
        );
        done();
      });
  });

  it('Invalid data rows should not return any results ', done => {
    sinon.restore();
    const stubbedFunc = sinon.stub(google, 'sheets');
    stubbedFunc.returns({
      spreadsheets: {
        values: {
          get: (_) => ({
            data: {
              values: [
                createRowHeader(),
                createInValidResourceRow(),
              ]
            }

          })
        }
      }
    });
    server
      .get(`${RESOUCE_URL}?resourceName=test`)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.message).to.be.empty;
        done();
      });
  });

  it('Valid data rows should return resources ', done => {
    sinon.restore();
    const stubbedFunc = sinon.stub(google, 'sheets');
    stubbedFunc.returns({
      spreadsheets: {
        values: {
          get: (_) => ({
            data: {
              values: [
                createRowHeader(),
                createValidResourceRow('test'),
              ]
            }

          })
        }
      }
    });
    server
      .get(`${RESOUCE_URL}?resourceName=test`)
      .expect(200)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        console.log(res.body.message);
        expect(res.body.message.toString()).to.equal([
          new Resource({
            resourceName: 'test',
            vendorName: 'Merhcant',
            address: 'Address',
            phoneNumbers: [ '+000', '9929' ],
            availabilityCount: '20',
            additionalComments: 'Additional comments',
            verified: true,
          })
        ].toString());
        done();
      });
  });
});
